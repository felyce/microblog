from fabric.api import cd, sudo, shell_env, run, env

HOST_PATH = "/opt/django/microblog/microblog"

ex_env = {
    "DJANGO_SETTINGS_MODULE": "microblog.settings.prod",
}

env.hosts = ["192.168.11.6", ]
env.user = "root"
env.password = "hogehoge"


def collect_static():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            run("/opt/django/microblog/bin/python manage.py collectstatic --no-input")


def git_update():
    with cd(HOST_PATH):
        run("git pull")


def restart():
    with cd(HOST_PATH):
        with shell_env(**ex_env):
            sudo("pkill -f gunicorn")

            sudo("/opt/django/microblog/bin/gunicorn -c gunicorn.conf.py microblog.wsgi")


def all():
    git_update()
    collect_static()
    restart()
